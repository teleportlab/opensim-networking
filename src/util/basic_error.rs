//! Bad error handling for cases where we would call unwrap otherwise.
//!
//! Basically this is really for convenience during development, and it's likely
//! that many instances can be replaced by more robust error handling in the
//! future.
//!
//! In the meantime this allows us to save on boilerplate error handling,
//! because this type can be used everywhere where we need a compatible error
//! type.

// TODO: Consider if this should also implement the fail trait.

use std::error::Error;
use std::fmt;

#[derive(Clone, Debug)]
pub struct BasicError(pub(crate) String);

impl BasicError {
    pub fn boxed(s: impl Into<String>) -> Box<BasicError> {
        Box::new(BasicError(s.into()))
    }
}

impl From<String> for BasicError {
    fn from(s: String) -> Self {
        BasicError(s)
    }
}

impl<'a> From<&'a str> for BasicError {
    fn from(s: &'a str) -> Self {
        BasicError(s.into())
    }
}

impl Error for BasicError {
    fn description(&self) -> &str {
        self.0.as_ref()
    }
}

impl fmt::Display for BasicError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "BasicError: {}", self.0)
    }
}
