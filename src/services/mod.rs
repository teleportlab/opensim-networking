use crate::capabilities::Capabilities;
use crate::circuit::{message_handlers, MessageSender};
use crate::logging::Log;
use crate::types::Uuid;
use std::cell::RefCell;
use std::sync::Arc;
use std::sync::Mutex;

pub trait Service {
    fn register_service(
        handlers: &mut message_handlers::Handlers,
        circuit_data: CircuitDataHandle,
        log: &Log,
    ) -> Self;
}

/// Provides access to the CircuitData once it is available.
///
/// At service registration time, the circuit connection won't be available yet.
/// It is guaranteed that the value will be available when handlers are first
/// invoked.
///
/// TODO: Find a better solution here, or at least make sure this does not cause
///       significant performance penalities.
#[derive(Clone)]
pub struct CircuitDataHandle(Arc<Mutex<Option<Arc<Mutex<CircuitData>>>>>);

assert_impl!(handle_is_sync; CircuitDataHandle, Send, Sync);

impl CircuitDataHandle {
    pub(crate) fn new() -> Self {
        CircuitDataHandle(Arc::new(Mutex::new(None)))
    }

    pub(crate) fn set(&self, data: CircuitData) {
        let mut option = self.0.lock().unwrap();
        *option = Some(Arc::new(Mutex::new(data)));
    }

    /// Return the available CircuitData.
    ///
    /// # Panics
    ///
    /// If data is not yet available. This should not happen outside the
    /// register_service methods.
    pub fn unwrap(&self) -> Arc<Mutex<CircuitData>> {
        let option = self.0.lock().unwrap();
        Arc::clone(option.as_ref().unwrap())
    }
}

pub struct CircuitData {
    pub capabilities: Capabilities,
    pub region_id: Uuid,
    pub message_sender: MessageSender,
}

pub mod region_handle;
pub mod terrain;
