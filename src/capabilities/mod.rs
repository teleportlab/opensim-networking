//! Capability API implementations.
//!
//! Unlike "services" capabilities use HTTP/s rather than the LLUDP protocol.

use futures::compat::Future01CompatExt;
use futures::prelude::*;
use futures01::Future;
use hyper;
use hyper::header::{CONTENT_LENGTH, CONTENT_TYPE};
use hyper::rt::Stream;
use llsd;
use std::collections::HashMap;
use tokio::runtime::TaskExecutor;
use url::Url;

#[derive(Clone, Debug)]
pub struct Capabilities {
    urls: HashMap<String, Url>,
}

#[derive(Debug, Fail)]
pub enum CapabilitiesError {
    #[fail(display = "Capability {} expected but not obtained.", 0)]
    NotPresent(String),

    #[fail(display = "capabilities error: {}", 0)]
    Msg(String),
}

impl Capabilities {
    // TODO: Offload to cpu executor.
    async fn build_request_body(val: llsd::data::Value) -> Result<Vec<u8>, CapabilitiesError> {
        let mut data = Vec::new();
        // data.write_all(&llsd::PREFIX_BINARY).unwrap();
        llsd::xml::write_doc(&mut data, &val).unwrap();
        Ok(data)
    }

    pub fn get_url(&self, cap: &str) -> Result<&Url, CapabilitiesError> {
        self.urls
            .get(cap)
            .ok_or_else(|| CapabilitiesError::NotPresent(cap.into()))
    }

    pub async fn setup_capabilities(
        seed_caps_uri: hyper::Uri,
        executor: TaskExecutor,
        requested_caps_names: Vec<&str>,
    ) -> Result<Capabilities, CapabilitiesError> {
        let requested_caps = llsd::data::Value::Array(
            requested_caps_names
                .iter()
                .map(|s| llsd::data::Value::new_string(*s))
                .collect(),
        );

        let client = hyper::Client::builder().executor(executor).build_http();

        let request_body = await!(Self::build_request_body(requested_caps))?;
        let request = hyper::Request::post(seed_caps_uri)
            .header(CONTENT_TYPE, "application/llsd+xml")
            .header(CONTENT_LENGTH, request_body.len())
            .body(hyper::Body::from(request_body))
            .map_err(|e| CapabilitiesError::Msg(format!("Constructing request failed: {}", e)))?;
        let response = await!(client.request(request).compat()).map_err(|e| {
            CapabilitiesError::Msg(format!("Setup capabilities request failed: {:?}", e))
        })?;

        if response.status().is_success() {
            {
                let c_type = response
                    .headers()
                    .get(CONTENT_TYPE)
                    .ok_or_else(|| CapabilitiesError::Msg("Content type not specified.".into()))?;
                if c_type
                    .to_str()
                    .map_err(|e| CapabilitiesError::Msg(format!("{}", e)))?
                    != "application/xml"
                {
                    return Err(CapabilitiesError::Msg(format!(
                        "wrong content type: {:?}",
                        c_type
                    )));
                }
            }

            // Read full response body.
            let raw_data = await!(response
                .into_body()
                .concat2()
                .map_err(|_| CapabilitiesError::Msg("Collecting body failed.".into()))
                .compat())?;
            let val = llsd::xml::read_value(&raw_data[..])
                .map_err(|_| CapabilitiesError::Msg("Invalid LLSD".to_string()))?;

            match val {
                llsd::data::Value::Map(mut map) => {
                    let mut urls = HashMap::new();

                    for cap_name in requested_caps_names {
                        let cap_url = map
                            .remove(cap_name)
                            .and_then(|v| v.scalar())
                            .and_then(|s| s.as_uri())
                            .and_then(|u| u.ok())
                            .ok_or_else(|| {
                                CapabilitiesError::Msg(format!("No {} cap.", cap_name))
                            })?;

                        urls.insert(cap_name.to_owned(), cap_url);
                    }

                    Ok(Capabilities { urls })
                }
                _ => Err(CapabilitiesError::Msg("LLSD is not a map.".into())),
            }
        } else {
            Err(CapabilitiesError::Msg("Response is error.".into()))
        }
    }
}
