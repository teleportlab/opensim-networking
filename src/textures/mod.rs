//! Contains the texture manager.
use crate::capabilities::Capabilities;
use crate::logging::Log;
use crate::types::Uuid;
use crate::util::basic_error::BasicError;
use futures::prelude::*;
use hyper;
use hyper::header::CONTENT_TYPE;
use slog::Logger;
use std::cell::RefCell;
use std::error::Error;
use std::io::Error as IoError;
use tokio::runtime::TaskExecutor;
use url::Url;

mod cache;
mod decode;

use self::cache::*;
use crate::capabilities::CapabilitiesError;

/// A handle to a texture which does not actually hold the texture data itself.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TextureHandle {
    id: Uuid,
    metadata: Option<TextureMetadata>,
}

/// Texture metadata.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TextureMetadata {
    width: u32,
    height: u32,
}

/// Instance of a texture including the metadata and binary data.
#[derive(Debug, Serialize, Deserialize)]
pub struct Texture {
    id: Uuid,
    metadata: TextureMetadata,
    data: Vec<u8>,
}

#[derive(Debug)]
pub enum TextureServiceError {
    DecodeError(Box<Error + Send + Sync>),

    IoError(IoError),

    /// There is an error with the sim configuration.
    ///
    /// Note: This is supposed to only happen in that case,
    ///       but technically it might also be an issue somewhere
    ///       in our code.
    SimConfigError(String),

    /// There was an error during network communication.
    NetworkError(String),
}

pub struct TextureService {
    get_texture: Url,
    caches: Vec<RefCell<TextureCache>>,
    executor: TaskExecutor,
    log: Log,
}

impl TextureHandle {
    pub fn id(&self) -> &Uuid {
        &self.id
    }

    pub fn metadata(&self) -> Option<&TextureMetadata> {
        self.metadata.as_ref()
    }
}

impl TextureMetadata {
    pub fn dimensions(&self) -> (u32, u32) {
        (self.width, self.height)
    }
}

impl Texture {
    pub fn id(&self) -> &Uuid {
        &self.id
    }
}

impl From<IoError> for TextureServiceError {
    fn from(e: IoError) -> Self {
        TextureServiceError::IoError(e)
    }
}

impl From<::jpeg2000::error::DecodeError> for TextureServiceError {
    fn from(e: ::jpeg2000::error::DecodeError) -> Self {
        TextureServiceError::DecodeError(Box::new(e))
    }
}

impl TextureService {
    pub fn new(
        caps: &Capabilities,
        executor: TaskExecutor,
        log: Log,
    ) -> Result<Self, CapabilitiesError> {
        Ok(TextureService {
            get_texture: caps.get_url("GetTexture")?.clone(),
            caches: Vec::new(),
            executor,
            log,
        })
    }

    /// Register a TextureCache as the next layer in the cache hierarchy.
    ///
    /// Caches will be queried on lookup in the order they were inserted here.
    pub fn register_cache(&mut self, cache: RefCell<TextureCache>) {
        self.caches.push(cache);
    }

    /// Get a texture by first checking the cache, then performing a network
    /// request if it was not found.
    pub async fn get_texture(&self, id: Uuid) -> Result<Texture, TextureServiceError> {
        // Get the texture from a cache if possible.
        // TODO: Currently this is performed with blocking IO.
        for cache_refcell in &self.caches {
            match cache_refcell.borrow_mut().get(&id) {
                Ok(Some(t)) => return Ok(t),
                _ => {}
            }
        }

        // Get the texture from the network instead.
        let url_res = self
            .get_texture
            .join(format!("?texture_id={}", id).as_str());
        let url = match url_res {
            Ok(u) => u,
            Err(_) => {
                return Err(TextureServiceError::SimConfigError(format!(
                    "get_texture url: {}",
                    self.get_texture
                )));
            }
        };

        let client: hyper::Client<
            hyper::client::HttpConnector<hyper::client::connect::dns::GaiResolver>,
            hyper::Body,
        > = hyper::Client::builder()
            .executor(self.executor.clone())
            .build_http();

        let logger = Logger::root(self.log.clone(), o!("texture request" => format!("{}",id)));
        debug!(logger, "request url: {:?}", url.clone());
        // TODO see: https://github.com/hyperium/hyper/issues/1219
        let uri: hyper::Uri = url.into_string().parse().unwrap();
        let resp = await!(client.get(uri))
            .map_err(|e| TextureServiceError::NetworkError(format!("{:?}", e)))?;

        debug!(logger, "received response");

        if resp.status().is_success() {
            // TODO: Support all relevant content types.
            let _content_type = match resp.headers().get(CONTENT_TYPE) {
                None => Err(TextureServiceError::NetworkError(
                    "No content type found.".to_string(),
                )),
                Some(raw_value) => {
                    let value = raw_value.to_str().map_err(|_| {
                        TextureServiceError::NetworkError("Header could not be decoded.".into())
                    })?;
                    match value {
                        "image/x-j2c" => Ok(()),
                        other => {
                            return Err(TextureServiceError::DecodeError(BasicError::boxed(
                                format!("Unsupported texture type: {:?}", other),
                            )));
                        }
                    }
                }
            };

            // TODO: Offload to executor thread! And don't read everything into memory.
            use futures::compat::Future01CompatExt;
            use futures01::{Future, Stream};
            let data = await!(resp.into_body().concat2())
                .map_err(|e| TextureServiceError::NetworkError(format!("{:?}", e)))?;

            // TODO: Perform the work on a thread pool!
            decode::extract_j2k(id, &data[..], self.log.clone())
                .map_err(|e| TextureServiceError::DecodeError(Box::new(e)))
        } else {
            Err(TextureServiceError::NetworkError(format!(
                "Sim returned status: {}",
                resp.status()
            )))
        }
    }
}
