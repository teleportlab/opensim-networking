use crate::logging::Log;
use crate::textures::{Texture, TextureMetadata};
use crate::types::Uuid;
use image::GenericImageView;
use jpeg2000;
use jpeg2000::decode::{Codec, ColorSpace, DecodeConfig};
use jpeg2000::error::DecodeError;

/// Extract JPEG2000 code stream.
pub fn extract_j2k(id: Uuid, raw_data: &[u8], log: Log) -> Result<Texture, DecodeError> {
    let config = DecodeConfig {
        default_colorspace: Some(ColorSpace::SRGB),
        discard_level: 0,
    };
    let image =
        jpeg2000::decode::from_memory(raw_data, Codec::J2K, config, Some(log.slog_logger()))?;

    Ok(Texture {
        id,
        metadata: TextureMetadata {
            width: image.width(),
            height: image.height(),
        },
        data: image.to_rgba().into_raw(),
    })
}
