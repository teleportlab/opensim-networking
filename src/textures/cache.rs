use crate::textures::Texture;
use crate::types::Uuid;
use simple_disk_cache::SimpleCache;

pub type TextureCache = SimpleCache<Uuid, Texture>;
