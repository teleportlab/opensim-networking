//! Contains the status types used for the circuit module.

use crate::types::SequenceNumber;

use futures::task::{LocalWaker, Waker};
use futures::Future;
use futures::Poll;
use std::pin::Pin;
use std::sync::{Arc, Mutex, RwLock};
use std::time::Instant;

/// The `Future` returned by the `Circuit`'s send method.
#[derive(Debug)]
pub struct SendMessage {
    status: Arc<RwLock<SendMessageStatus>>,
    waker: Arc<Mutex<Option<Waker>>>,
}

assert_impl!(SendMessage; SendMessage, Send, Sync);

impl SendMessage {
    pub(crate) fn new(status: SendMessageStatus) -> Self {
        SendMessage {
            status: Arc::new(RwLock::new(status)),
            waker: Arc::new(Mutex::new(None)),
        }
    }

    pub(crate) fn update_status(&mut self, new_status: SendMessageStatus) {
        *self.status.write().unwrap() = new_status;

        let waker = self.waker.lock().unwrap();
        match *waker {
            Some(ref w) => w.wake(),
            None => {}
        }
    }

    pub(crate) fn get_status(&self) -> SendMessageStatus {
        self.status.read().unwrap().clone()
    }
}

impl Clone for SendMessage {
    fn clone(&self) -> Self {
        SendMessage {
            status: Arc::clone(&self.status),
            waker: Arc::clone(&self.waker),
        }
    }
}

impl Future for SendMessage {
    /// If we complete without error it means an unreliable packet was sent or
    /// a reliable packet was sent and ack'ed by the server.
    type Output = Result<(), SendMessageError>;

    fn poll(self: Pin<&mut Self>, lw: &LocalWaker) -> Poll<Self::Output> {
        match *self.status.read().unwrap() {
            SendMessageStatus::PendingSend { .. } | SendMessageStatus::PendingAck { .. } => {
                let mut waker = self.waker.lock().unwrap();
                if waker.is_none() {
                    *waker = Some(lw.clone().into_waker());
                }
                Poll::Pending
            }
            SendMessageStatus::Success => Poll::Ready(Ok(())),
            SendMessageStatus::Failure(err) => Poll::Ready(Err(err)),
        }
    }
}

// TODO: This seems kind of stupid.
#[derive(Debug, Clone, Copy)]
pub enum SendMessageError {
    /// Remote failed to acknowledge the packet.
    FailedAck,
}

impl ::std::fmt::Display for SendMessageError {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        write!(f, "ack failed.")
    }
}

impl ::std::error::Error for SendMessageError {
    fn description(&self) -> &str {
        "Ack failed."
    }
}

/// Describes the current status of a message that was submitted to be sent.
#[derive(Debug, Clone, Copy)]
pub enum SendMessageStatus {
    /// Has not been sent through the socket yet.
    PendingSend { reliable: bool },
    /// Has been sent but not acknowledged yet.
    /// The attempt variant describes the number of attempts already made.
    /// (0 → this is the first attempt, 1 → 2nd attempt, etc.)
    /// timeout: holds the time after which the current attemt is considered
    /// timed out.
    PendingAck {
        attempt: u8,
        timeout: Instant,
        id: SequenceNumber,
    },
    /// Has finished successfully.
    Success,
    /// Has failed.
    Failure(SendMessageError),
}

impl SendMessageStatus {
    pub fn is_failure(&self) -> bool {
        match *self {
            SendMessageStatus::Failure(_) => true,
            _ => false,
        }
    }
}
