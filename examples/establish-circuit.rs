#![feature(async_await, await_macro, futures_api)]
#![recursion_limit = "128"]

extern crate futures;
extern crate futures01;
extern crate opensim_networking;
#[macro_use]
extern crate serde_derive;
extern crate tokio;
extern crate toml;

use opensim_networking::circuit::message_handlers;
use opensim_networking::logging::{Log, LogLevel};
use opensim_networking::login::{hash_password, LoginRequest};
use opensim_networking::simulator::{ConnectInfo, Simulator};
use opensim_networking::systems::agent_update::{AgentState, Modality, MoveDirection};
use opensim_networking::types::{Duration, Instant, UnitQuaternion, Vector3};

use futures::compat::{Compat, Future01CompatExt, Stream01CompatExt};
use std::fs::File;
use std::io::prelude::*;
use tokio::prelude::*;
use tokio::runtime::{current_thread, Runtime};
use tokio::timer::Interval;

#[derive(Deserialize)]
struct Config {
    user: ConfigUser,
    sim: ConfigSim,
}

#[derive(Deserialize)]
struct ConfigUser {
    first_name: String,
    last_name: String,
    password_plain: String,
}

#[derive(Deserialize)]
struct ConfigSim {
    loginuri: String,
}

fn main() {
    // Setup logging.
    let log = Log::new_dir("output/logdir", LogLevel::Debug).expect("Setting up log failed.");

    // Read the configuration file.
    let config = get_config();

    // Perform the login.
    let request = LoginRequest {
        first_name: config.user.first_name,
        last_name: config.user.last_name,
        password_hash: hash_password(config.user.password_plain.as_str()),
        start: "last".to_string(),
    };

    println!("Performing login request: {:?}", request);
    let resp = request
        .perform(config.sim.loginuri.as_str())
        .expect("Login failed.");
    // println!("Login success, response = {:?}", resp);
    println!("Login success.");
    let agent_id = resp.agent_id.clone();
    let session_id = resp.session_id.clone();

    // Relevant issue: https://github.com/tokio-rs/tokio/issues/382

    // TODO: could also use block_on_all instead without timeouts
    // TODO: remove the futures lazy if it does not help against the spawnerror.

    current_thread::run(Compat::new(Box::pin(
        async move {
            let runtime = Runtime::new().unwrap();

            let message_handlers = message_handlers::Handlers::default();
            let sim_connect_info = ConnectInfo::from(resp);

            let sim = await!(Simulator::connect(
                sim_connect_info,
                message_handlers,
                runtime.executor(),
                log
            ))
            .unwrap();

            // Exemplary texture request.
            let texture_id = sim.region_info().terrain_detail[0].clone();
            let texture = await!(sim.get_texture(texture_id)).unwrap();
            println!("texture received: {}", texture.id());

            // Let the avatar walk back and forth.
            // TODO: extract position
            let z_axis = Vector3::z_axis();
            let mut state = AgentState {
                position: Vector3::new(0., 0., 0.),
                move_direction: Some(MoveDirection::Forward),
                modality: Modality::Walking,
                // TODO: This initialization is redundant as it was already done in
                // simulator.rs
                body_rotation: UnitQuaternion::from_axis_angle(&z_axis, 0.),
                head_rotation: UnitQuaternion::from_axis_angle(&z_axis, 0.),
            };

            let mut interval = Interval::new(Instant::now(), Duration::from_millis(200));

            loop {
                for _ in 1i32..40 {
                    let msg = state.to_update_message(agent_id, session_id);
                    // TODO: change this to unreliable (false) after debugging
                    await!(sim.send_message(msg, true)).unwrap();
                    println!("sent message to sim successfully");

                    await!(interval.next()).unwrap().unwrap();
                }

                await!(interval.next()).unwrap().unwrap();
                state.move_direction = Some(state.move_direction.unwrap().inverse());
            }

            Ok(())
        },
    )));
}

fn get_config() -> Config {
    let mut file = File::open("establish-circuit.toml")
        .expect("Copy establish-circuit.toml.tpl to establisk-circuit.toml and populate it.");
    let mut raw_data = String::new();
    file.read_to_string(&mut raw_data).unwrap();
    toml::from_str(raw_data.as_str()).expect("invalid TOML")
}
